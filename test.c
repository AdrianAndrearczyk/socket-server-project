#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>

#define ROZMIAR_BUFORA 1024
typedef struct {
	char chatBuffer[ROZMIAR_BUFORA];
	int licznik;
} mybuffer;


void program (int sock, mybuffer *b);
void not_logged (int sock);
void already_logged (int sock);
void doesnt_exist (int sock);


int fputs( const char *s, FILE *fp );
int x = 1;



int main( int argc, char *argv[] ) {
   mybuffer *b; // bufor w pamięci współdzielonej
	key_t keyval = ftok(argv[0],1234);
	//printf("%d %x\n", (int)sizeof(mybuffer),(int)keyval);
	int shmid = shmget( keyval, sizeof(mybuffer), IPC_CREAT | 0660 ); // przygotowanie pamieci wspoldzielonej
	b = (mybuffer *)shmat(shmid, 0, 0);
	memset(b,0,sizeof(mybuffer));
   signal(SIGPIPE, SIG_IGN);
   int sockfd, newsockfd, portno, clilen;
   char buffer[1024];
   struct sockaddr_in serv_addr, cli_addr;
   int n;
   int pid;
   b->licznik = 0;
   
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }


   bzero((char *) &serv_addr, sizeof(serv_addr));
	int enable = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
    error("setsockopt(SO_REUSEADDR) failed");

   portno = 1337;
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(portno);
   memset(&(serv_addr.sin_zero), 0, 8);
   

   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      exit(1);
   }
   
   listen(sockfd,5);
   signal(SIGCHLD, SIG_IGN);
   clilen = sizeof(cli_addr);
   
   while (x == 1) {
      newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
      
	    unsigned char *addr = 
		(char*)&(cli_addr.sin_addr.s_addr);
		printf("Otrzymalem polaczenie z adresu: %d.%d.%d.%d\n",
		addr[0],addr[1],addr[2],addr[3]);
      
      if (newsockfd < 0) {
         perror("ERROR on accept");
         exit(1);
      }

      pid = fork();
      
      if (pid < 0) {
         perror("ERROR on fork");
         exit(1);
      }

      
      if (pid == 0) {
         close(sockfd);
         program(newsockfd, b);
         
         exit(0);
      }
      else {
         close(newsockfd);
      }
		
   }
   close(sockfd);
   	shmdt ( b );
	shmctl ( shmid, IPC_RMID, 0 );
}


void program (int sock, mybuffer *b) 
{
		send(sock, "\033[H\033[J", 7, 0);
		const int NOT_AUTH = 0;
		const int LOGGED_IN = 1;
		char buffer[1024];
		
		strcpy(buffer, "Witaj.\nUzyj /help aby wyswietlic dostepne komendy\n");
		send(sock, buffer, strlen(buffer), 0);

		int state = NOT_AUTH;
			while(1)
			{
				recv(sock, buffer, 1024, 0);
				
				if(!strncmp("/exit", buffer, 5))
				{
					close(sock);
					return;
				}
				else if(!strncmp("/help", buffer, 5))
				{
					
					strcpy(buffer, "\n/login - sluzy do logowania sie do systemu.\n/help - wyswietla ta liste komend.\n/stworz - tworzy/dopisuje tekst do pliku.\n/otworz - otwiera plik z logami i wyswietla jego zawartosc.\n/exit - wylacza klienta.\n\n");
					send(sock, buffer, strlen(buffer), 0);
				}
				else if(!strncmp("/stworz", buffer, 7))
				{
					if(state !=1) { not_logged(sock); continue;}
					FILE *fp;
					char text[1024];
					fp = fopen("log.txt", "a");
					sscanf(buffer + 8, "%[^\n]s", text);
					fputs(text, fp);
					fputs("\n", fp);
					fclose(fp);
				}
				
				else if(!strncmp("/otworz", buffer, 7))
				{
					if(state !=1) { not_logged(sock); continue;}
					FILE *fp;
					fp = fopen("log.txt", "r");
					if(fp == NULL) { doesnt_exist(sock); continue;}
					char text[1024];
					while(fgets(text, 100, fp)) {
    				strcpy(buffer, text);
    				send(sock, buffer, strlen(buffer), 0);
					}

				}
				else if(!strncmp("/pisz", buffer, 5))
				{
					//if(state !=1) { not_logged(sock); continue;}

					char text[1024];
					sscanf(buffer + 6, "%[^\n]s", text);
    				strcpy(b->chatBuffer, text);
    				send(sock, b->chatBuffer, strlen(b->chatBuffer), 0);
    				b->licznik = 1;
					}
				else if(!strncmp("/pokaz", buffer, 6))
				{
					//if(state !=1) { not_logged(sock); continue;}
					char text[1024];
    				strcpy(text, b->chatBuffer);
    				send(sock, text, strlen(text), 0);
    				b->licznik = 0;
					}
				
				
				
				else if(!strncmp("/login", buffer, 6))
				{
					if(state == 1) { already_logged(sock); continue; }
					int temp = 0;
					char username[255];
					sscanf(buffer + 7, "%s ", username);
					FILE *fp;
					fp = fopen("users.txt", "r");
					char string[100];
					char g[100];
					while(fgets(string, 100, fp)) {
						strcpy(g,string);
    					if(strcmp(g, username) > 0){
						temp = 1;
						state = LOGGED_IN;
						strcpy(buffer, "Logowanie udane. Witaj ");
						send(sock, buffer, strlen(buffer), 0);
						strcpy(buffer, g);
						send(sock, buffer, strlen(buffer), 0);
						printf("Wykryto logowanie uzytkownika: %s \n", g);
						break;
						}
					}
					if(temp == 0) printf("Uzytkownik %s nie istnieje.\n", username);

				}
				else
				{
					strcpy(buffer, "Komenda nie rozpoznana. Sprobuj ponownie\n");
					send(sock, buffer, strlen(buffer), 0);
				}
			}
			}

		
void not_logged(int sock)
{
	char buffer[1024];
	strcpy(buffer, "Nie masz uprawnien do uzycia tej komendy. Zaloguj sie.\n");
	send(sock, buffer, strlen(buffer), 0);
}		

void already_logged(int sock)
{
	char buffer[1024];
	strcpy(buffer, "Jestes juz zalogowany.\n");
	send(sock, buffer, strlen(buffer), 0);
}

void doesnt_exist(int sock)
{
	char buffer[1024];
	strcpy(buffer, "Plik z danymi nie istnieje. Uzyj komendy /stworz aby stworzyc plik z logami.\n");
	send(sock, buffer, strlen(buffer), 0);
}
void sig_handler(int signo)
{
  if (signo == SIGINT)
  x = 0;
}
