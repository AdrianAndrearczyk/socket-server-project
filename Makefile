
all: bin/libplik.so

bin/libplik.so: src/plik.o
	mkdir -p bin
	gcc -o bin/libplik.so -shared src/plik.o
	
src/plik.o: src/plik.c
	gcc -Iinclude -c src/plik.c -fpic
	mv deco.o src/deco.o

install: bin/libplik.so
	mkdir -p /usr/lib
	cp bin/libplik.so /usr/lib/
	cp include/plik.h /usr/include/
	echo '/usr/lib' >  /etc/ld.so.conf.d/plik.conf
	ldconfig

uninstall: 	
	rm /usr/lib/libplik.so
	rm /usr/include/plik.h
	rm /etc/ld.so.conf.d/plik.conf
	ldconfig

clean:
	rm -f src/*.o
	rm -f bin/libplik.so
	
